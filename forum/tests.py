from django.test import TestCase


class UnitTest(TestCase):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_access_homepage(self):
        response = self.client.get("/")
        self.assertAlmostEquals(response.status_code, 200)
    