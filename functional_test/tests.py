from django.test import TestCase
from selenium import webdriver
import time

class NewVisitTest(TestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver'
        )

    def tearDown(self):
        self.selenium.implicitly_wait(10)
        self.selenium.quit()
        super().tearDown()

    def test_can_post_a_status_and_see_it_on_the_homepage(self):   
        # Tina just read a news that mentioned a new web-based social media
        # She goes to the web
        self.selenium.get("localhost:8000/")
        time.sleep(10)
        # The app named enViralle. She recognized it by the webpage's title.
        self.assertIn("enViralle", self.selenium.title)
        
        # Tina sees some writings from other users.
        # Oh, so it looks like an open forum. People can freely post their thoughts on the web.
        # One of them is about remission for corruptors due to spread of Covid-19, by Najwa Shihab

        # Tina also wants her thoughts appear on the website.
        # She looks at a simple form. She is asked to enter her name

        # And to share write her thought

        # So now, she types her fullname, "Karan Tina"

        # And also her thought on the form

        # She gets directed to a confirmation page

        # She clicks yes, and gets redirected back to the homepage

        # Now she sees her writing there
