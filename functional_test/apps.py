from django.apps import AppConfig


class FunctionalTestConfig(AppConfig):
    name = 'functional_test'
